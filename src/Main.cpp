/* Amir Hossein Firouzian
 * Student No. 95111005
 * This Software is to compute temperature for
 * simple plate with we have boundary condition
 * for it.
 */

#include <iostream>
using std::cout;
using std::endl;

#include <fstream>

// Only for fabs() Function.
#include <cmath>
#include "Functions.h"

// Number of points 
#define Number_Of_Edge_Points 9

int main()
{
  // Create Matrix to save temperature
  double **Points = new double*[Number_Of_Edge_Points];
  for (int Loop=0; Loop< Number_Of_Edge_Points; Loop++)
    Points[Loop]=new double[Number_Of_Edge_Points];
  
  /* Call function and pass boundary condition to solve.
   * This function give error as well. So it terminate
   * after some time.
  */
  Compute_Temperature(Points, 400, 300, 200, 500, 0.01);
  
  for (int Loop_1=0; Loop_1 < Number_Of_Edge_Points; Loop_1++){
    for (int Loop_2=0; Loop_2 < Number_Of_Edge_Points; Loop_2++){
      cout << Points[Loop_1][Loop_2] << "\t";
    }
  cout << endl;
  }

  // Delete created Matrix
  for (int Loop=0; Loop< Number_Of_Edge_Points; Loop++)
    delete [] Points[Loop];
  delete Points;
}


// Function which Compute temperature.
void Compute_Temperature(double **Matrix_Of_Points,
                         double Top_Edge_Temperature,
                         double Right_Edge_Temperature,
                         double Bottom_Edge_Temperature,
                         double Left_Edge_Temperature,
                         double Error)
{
  /* Initiate Matrix with Zero to start Gauss–Seidel; And set boundry 
   * condition as well
  */
  for (int Loop_1=0; Loop_1<Number_Of_Edge_Points; Loop_1++){
    for (int Loop_2=0; Loop_2 < Number_Of_Edge_Points; Loop_2++){
      if (0<Loop_1 && 0<Loop_2 && Loop_1 < Number_Of_Edge_Points-1 && 
          Loop_2 < Number_Of_Edge_Points-1)
        Matrix_Of_Points[Loop_1][Loop_2]=0;
      
      else if (Loop_1==0)
        Matrix_Of_Points[Loop_1][Loop_2]=Top_Edge_Temperature;
      
      else if (Loop_2==Number_Of_Edge_Points-1)
        Matrix_Of_Points[Loop_1][Loop_2]=Right_Edge_Temperature;
      
      else if (Loop_1==Number_Of_Edge_Points-1)
        Matrix_Of_Points[Loop_1][Loop_2]=Bottom_Edge_Temperature;
      
      else if (Loop_2==0)
        Matrix_Of_Points[Loop_1][Loop_2]=Left_Edge_Temperature;
    }
  }
  
  double Max_Error=0;
  do {
    Max_Error=0;
    
    // This New Matrix to save new temperature data.
    double **New_Temperature = new double*[Number_Of_Edge_Points];
    for (int Loop=0; Loop< Number_Of_Edge_Points; Loop++)
      New_Temperature[Loop]=new double[Number_Of_Edge_Points];

    // Gauss–Seidel Part.
    for (int Loop_1=1; Loop_1 < Number_Of_Edge_Points-1; Loop_1++){
      for (int Loop_2=1; Loop_2 < Number_Of_Edge_Points-1; Loop_2++)
        New_Temperature[Loop_1][Loop_2]=(Matrix_Of_Points[Loop_1+1][Loop_2]+
                                         Matrix_Of_Points[Loop_1][Loop_2+1]+
                                         Matrix_Of_Points[Loop_1-1][Loop_2]+
                                         Matrix_Of_Points[Loop_1][Loop_2-1])/4;
    }

    // Compute Error
    for (int Loop_1=1; Loop_1 < Number_Of_Edge_Points-1; Loop_1++){
      for (int Loop_2=1; Loop_2 < Number_Of_Edge_Points-1; Loop_2++){
        double Temp_Error=New_Temperature[Loop_1][Loop_2]-
                          Matrix_Of_Points[Loop_1][Loop_2];
        Temp_Error=fabs(Temp_Error);
        Matrix_Of_Points[Loop_1][Loop_2]=New_Temperature[Loop_1][Loop_2];
      
        if (Temp_Error>Max_Error)
          Max_Error=Temp_Error;
      }
    }
    // Delete created Matrix
    for (int Loop=0; Loop< Number_Of_Edge_Points; Loop++)
      delete [] New_Temperature[Loop];
    delete New_Temperature;
    
    // cout << Max_Error <<endl;
    
  } while (Max_Error>=Error);
}
